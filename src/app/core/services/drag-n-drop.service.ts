/**
 * Project: life BCG
 * File: drag-n-drop.service
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { Observable } from 'rxjs';


export interface FileSelectResult {

  /** The added files, emitted in the filesAdded event. */
  addedFiles: File[];

  /** The rejected files, emitted in the filesRejected event. */
  rejectedFiles: RejectedFile[];

}

export interface RejectedFile extends File {

  /** The reason the file was rejected. */
  reason?: RejectReason;
}

export type RejectReason = 'type' | 'size' | 'no_multiple';

/**
 * This service contains the filtering logic to be applied to
 * any dropped or selected file. If a file matches all criteria
 * like maximum size or accept type, it will be emitted in the
 * addedFiles array, otherwise in the rejectedFiles array.
 */
@Injectable({
  providedIn: 'root'
})
export class DragNDropService {

  private render: Renderer2;

  constructor(
    rendererFactory: RendererFactory2
  ) {
    this.render = rendererFactory.createRenderer(null, null);
  }


  parseFileList(files, accept: string, maxFileSize: number, multiple: boolean): FileSelectResult {

    const addedFiles: File[] = [];

    const rejectedFiles: RejectedFile[] = [];

    for (let i = 0; i < files.length; i++) {
      const file = files.item(i);

      // Object.defineProperty(file, 'name', {
      //   writable: true,
      //   value: 'sushant'
      // }); // for changing file name

      // this.readFile(file)
      // .then(img => setTimeout(() => { 

      // }))
      // .catch(err => console.error(err));


      if (!this.isAccepted(file, accept)) {
        this.rejectFile(rejectedFiles, file, 'type');
        continue;
      }

      if (maxFileSize && file.size > maxFileSize) {
        this.rejectFile(rejectedFiles, file, 'size');
        continue;
      }

      if (!multiple && addedFiles.length >= 1) {
        this.rejectFile(rejectedFiles, file, 'no_multiple');
        continue;
      }

      addedFiles.push(file);

    }


    const result = {
      addedFiles,
      rejectedFiles,
    };

    return result;
  }

  private isAccepted(file: File, accept: string): boolean {

    if (accept === '*') {
      return true;
    }

    const acceptFiletypes = accept.split(',').map(it => it.toLowerCase().trim());
    const filetype = file.type.toLowerCase();
    const filename = file.name.toLowerCase();

    const matchedFileType = acceptFiletypes.find(acceptFiletype => {

      // check for wildcard mimetype (e.g. image/*)
      if (acceptFiletype.endsWith('/*')) {
        return filetype.split('/')[0] === acceptFiletype.split('/')[0];
      }

      // check for file extension (e.g. .csv)
      if (acceptFiletype.startsWith(".")) {
        return filename.endsWith(acceptFiletype);
      }

      // check for exact mimetype match (e.g. image/jpeg)
      return acceptFiletype == filetype;
    });

    return !!matchedFileType;
  }

  private rejectFile(rejectedFiles: RejectedFile[], file: File, reason: RejectReason) {

    const rejectedFile = file as RejectedFile;
    rejectedFile.reason = reason;

    rejectedFiles.push(rejectedFile);
  }


  /**
   * readFile is use to get blob from file object
   */
  readFile(file): Promise<string | ArrayBuffer> {
    return new Promise<string | ArrayBuffer>((resolve, reject) => {
      const reader = new FileReader();

      reader.onload = e => {
        resolve((e.target as FileReader).result);
      };

      reader.onerror = e => {
        console.error(`FileReader failed on file ${file.name}.`);
        reject(e);
      };

      if (!file) {
        return reject('No file to read. Please provide a file using the [file] Input property.');
      }

      reader.readAsDataURL(file);
    });
  }


  /**
   * compress is use to compress image file based on given ratio
   */
  compress(file: File, ratio: number, accept: string, maxFileSize: number): Observable<any> {

    if (!this.isAccepted(file, accept)) {
      return Observable.create(observer => {
        observer.next()
      })
    }

    if (maxFileSize && file.size > maxFileSize) {
      return Observable.create(observer => {
        observer.next()
      })
    }

    const reader = new FileReader();
    ratio = ratio / 100;
    reader.readAsDataURL(file);
    return Observable.create(observer => {
      reader.onload = ev => {
        const img = new Image();
        img.src = (ev.target as any).result;
        (img.onload = () => {
          const elem: HTMLCanvasElement = this.render.createElement('canvas');

          let w, h;
          w = img.naturalWidth;
          h = img.naturalHeight;

          elem.width = w * ratio;
          elem.height = h * ratio;

          const ctx: CanvasRenderingContext2D = elem.getContext('2d');

          ctx.drawImage(img, 0, 0, elem.width, elem.height);

          ctx.canvas.toBlob(
            blob => {
              observer.next(
                new File([blob], file.name, {
                  type: file.type,
                  lastModified: Date.now(),
                }),
              );
            },
            file.type,
            1,
          );
        }),
          (reader.onerror = error => observer.error(error));
      };
    });
  }

}

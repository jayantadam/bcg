import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Offices, FunctionCohort, Group, Category, User } from "@models/index";
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CreateCategoryService {

  constructor(private http: HttpClient) { }

  public getOffices$(): Observable<Offices> {
    const url = `${environment.apiBaseUrl}/office`;
    return this.http.get<Offices>(url);
  }

  public getFunctionCohort$(): Observable<FunctionCohort> {
    const url = `${environment.apiBaseUrl}/function`;
    return this.http.get<FunctionCohort>(url);
  }

  public getFunctionMix$(): Observable<FunctionCohort> {
    const url = `${environment.apiBaseUrl}/function/mix`;
    return this.http.get<FunctionCohort>(url);
  }

  public getGroup$(): Observable<Group> {
    const url = `${environment.apiBaseUrl}/group`;
    return this.http.get<Group>(url);
  }

  public getCategory$(): Observable<Category> {
    const url = `${environment.apiBaseUrl}/category/temp`;
    return this.http.get<Category>(url);
  }

  public getEmployee$(set, all_attr, user_type, limit): Observable<User> {
    // const  params = new  HttpParams().
    // set('set', set).
    // set('all_attr', all_attr).
    // set('user_type', user_type).
    // set('limit', limit);

    const url = `${environment.apiBaseUrl}/user`;
    // return this.http.get<User>(url,{ params }); 
    return this.http.get<User>(url);
  }
  
  public uploadGallery$(body : any[]): Observable<any> {
    
    const formData = new FormData();
    
    formData.append('category_id', 'ff1bf725-ef3a-476a-b2ce-cb0bc75b14f0');
    for (var i = 0; i < body.length; i++) {
      // formData.append('gallery[' + i + '][name]', arr[i].name);
      // formData.append('gallery[' + i + '][description]', arr[i].desc);
      formData.append('file_0', body[i]);
    }

    const url = `${environment.apiBaseUrl}/category/gallery`;
    // return;
    return this.http.post<any>(url, formData);
  }

  public addCategory$(categoryData): Observable<any> {
    const url = `${environment.apiBaseUrl}/category`;
    return this.http.post<any>(url,categoryData);
  }
}

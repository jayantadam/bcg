/**
 * Project: life BCG
 * File: spinner.service
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  constructor(private spinner: NgxSpinnerService) { }

  /**
   * Used for the Showing The Spinner by name
   * @param name name of the spinner
   */
  showSpinnerByName(name: string) {
    this.spinner.show(name);
  }

  /**
   * Used for the Hideing The Spinner by name
   * @param name name of the spinner
   */
  hideSpinnerByName(name: string) {
    this.spinner.hide(name);
  }

  /**
   * Used for the Showing The Loader
   */
  showSpinner() {
    this.spinner.show();
  }

  /**
   * Used for the Hideing The Loader
   */
  hideSpinner() {
    this.spinner.hide();
  }

}

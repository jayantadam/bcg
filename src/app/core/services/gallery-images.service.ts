import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GalleryImagesService {

  filesData = [];
  constructor() { }

  setGalleryData(data) {
    this.filesData = data;
  }

  getGalleryData() {
    return this.filesData;
  }
}

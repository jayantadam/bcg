import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';

export interface post {
  ids: Number;
  name: Number;
  age: Number;
}

@Injectable({
  providedIn: 'root'
})
export class WebService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * This method is for Get Api Calls
   * @param endPoint String Just need to pass the Endpoint
   * @returns This function returns the respective response from the Api
   */
  getApi(endPoint): Observable<any> {
    
    return this.http.get(endPoint);
    
    // return this.http.get(endPoint).pipe(
    //   retry(1), // retry a failed request up to 3 times
    //   catchError(() => of({ type: '[Movies API] Movies Loaded Error' })) // then handle the error
    // );
    // return temp;
  }

  postEmployee(endPoint, data): Observable<any>{
    return this.http.post(endPoint,data);
  }
  
}

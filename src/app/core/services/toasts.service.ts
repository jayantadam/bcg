/**
 * Project: life BCG
 * File: toasts.service
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Injectable } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';

@Injectable({
  providedIn: 'root'
})
export class ToastsService {

  toastsOption = {
    animate: 'slideFromRight',
    showCloseButton:false,
    toastTimeout:3000,
    dismiss:'auto',
    position:'top-right'
  }
  constructor(private toastr: ToastrManager) { }

  /**
   * Used for the Showing The Toasts
   * @param message message for toasts
   */
  showSuccess(message: string) {
    this.toastr.successToastr(message, 'Done!', this.toastsOption);
  }

  /**
   * Used for the Showing The Toasts
   * @param message message for toasts
   */
  showError(message: string) {
    this.toastr.errorToastr(message, 'Oops...', this.toastsOption);
  }

  /**
   * Used for the Showing The Toasts
   * @param message message for toasts
   */
  showWarning(message: string) {
    this.toastr.warningToastr(message, 'Warning!', this.toastsOption);
  }

  /**
   * Used for the Showing The Toasts
   * @param message message for toasts
   */
  showInfo(message: string) {
    this.toastr.infoToastr(message, 'Information', this.toastsOption);
  }

}

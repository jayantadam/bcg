/**
 * Project: life BCG
 * File: logger.service
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Injectable, ErrorHandler } from '@angular/core';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor(private errorHandler: ErrorHandler) { }
  
  /**
   * Used for the print a console log
   * @param value value to print
   */
  log(value: any, ...rest: any[]) {
    if(!environment.production)
      console.log('LoggerService: ',value, ...rest);
  }

  /**
   * Used for the print a error log
   * @param value value to print
   */
  error(value: any, ...rest: any[]) { 
    // this.errorHandler.handleError(value);
    console.error('LoggerService: ', value, ...rest);
  }

  /**
   * Used for the print a warning log
   * @param value value to print
   */
  warn(value: any, ...rest: any[]) { 
    console.warn('LoggerService: ',value, ...rest); 
  }
}

/**
 * Project: life BCG
 * File: header.interceptor
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
    constructor() {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let reqUrl = environment.apiBaseUrl;
        // let loggedInUser = this.authenticationService.loggedInUserValue;
        // if (loggedInUser && loggedInUser.token) {
            req = req.clone({
                setHeaders: { 
                    // 'Accept': '*/*',
                    
                    // Authorization: `Bearer ${loggedInUser.token}`
                  // 'Content-Type':  'multipart/form-data; boundary=----WebKitFormBoundarySeWrwh0bieIBGtco',
                },
                // url : reqUrl+req.url
            });
        // }

        return next.handle(req);

    }
}
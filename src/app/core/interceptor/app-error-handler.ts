/**
 * Project: life BCG
 * File: app-error-handler
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { ErrorHandler } from '@angular/core';
import { Injectable, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { ErrorService } from '@services/error.service';
import { LoggerService } from '@services/logger.service';
import { ToastsService } from '@services/toasts.service';


@Injectable({ providedIn: 'root' })
export class AppErrorHandler implements ErrorHandler {
    
    constructor(private injector: Injector) { }

    handleError(error) {
        
        const errorService = this.injector.get(ErrorService);
        const loggerService = this.injector.get(LoggerService);
        const toastsService = this.injector.get(ToastsService);

        let message;
        let stackTrace;

        if (error instanceof HttpErrorResponse) {
            // Server Error
            message = errorService.getServerMessage(error);
            stackTrace = errorService.getServerStack(error);
            toastsService.showError(message);
        } else {
            // Client Error
            message = errorService.getClientMessage(error);
            stackTrace = errorService.getClientStack(error);
            // notifier.showError(message);
        }

        // Always log errors
        loggerService.error(message,stackTrace);
    }
}

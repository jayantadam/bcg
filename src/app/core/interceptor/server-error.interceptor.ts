/**
 * Project: life BCG
 * File: server-error.interceptor
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ServerErrorInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(catchError(err => {
            
            // Handle the error cases based on status code
            if (err.status === 401 && err.error !== undefined) { // auto logout if 401 response returned from api
                
                // this.authenticationService.logout();
                // this.router.navigateByUrl('/auth/login');
                // redirect to login
            }
            
            return throwError(err);
        }))
    }
}



import { ActionReducerMap } from '@ngrx/store';

import {categoryReducer,polls} from './category/reducers/category.reducer';
import {surveyReducer,survey} from './category/reducers/survey.reducer'
import {appUrlReducer,appurl} from './category/reducers/appurl.reducer'

export interface IAppState {
    polls:polls;
    survey:survey;
    appurl:appurl
}

export const appReducers: ActionReducerMap<IAppState> = {
polls: categoryReducer,
survey: surveyReducer,
appurl: appUrlReducer
};
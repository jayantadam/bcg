import * as fromData from "../actions/category.action";

export interface survey{
    surveyData:any
  }

  export const initialState:survey={
    surveyData:[]
  }

  export function surveyReducer(
    state = initialState,
    action: fromData.ActionsUnion
  ): survey {
    switch (action.type) {
      case fromData.ActionTypes.SurveysData:
         {
        return {
          ...state,surveyData:state.surveyData.concat({...action.payload.surveyData})
        };
      };
      break;
  
    //   case fromData.ActionTypes.removeData:
    //     {   
    //       const commentId = action.unique_id;
    //       return{
    //         ...state,pollsData:state.pollsData.filter(comment => comment.unique_id !== commentId)       
    //       }        
    //     }
  
      default: {
        return state;
      }
      
    }
  }
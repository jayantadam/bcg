import * as fromData from "../actions/category.action";

export interface appurl{
    appUrlData:any
  }

  export const initialState:appurl={
    appUrlData:[]
  }

  export function appUrlReducer(
    state = initialState,
    action: fromData.ActionsUnion
  ): appurl {
    switch (action.type) {
      case fromData.ActionTypes.AppUrlData:
         {
        return {
          ...state,appUrlData:state.appUrlData.concat({...action.payload.appUrlData})
        };
      };
      break;

      default: {
        return state;
      }
      
    }
  }
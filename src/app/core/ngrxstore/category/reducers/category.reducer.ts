import * as fromData from "../actions/category.action";

export interface polls {
  pollsData:any
}

export const initialState: polls = {
    pollsData:[]
};

export function categoryReducer(
  state = initialState,
  action: fromData.ActionsUnion
): polls {
  switch (action.type) {
    case fromData.ActionTypes.Polls_CategoryData:
       {
      return {
        ...state,pollsData:state.pollsData.concat({...action.payload.pollsData})
      };
    };
    break;

    case fromData.ActionTypes.removeData:
      {   
        const commentId = action.unique_id;
        return{
          ...state,pollsData:state.pollsData.filter(comment => comment.unique_id !== commentId)       
        }        
      }

    default: {
      return state;
    }
    
  }
}
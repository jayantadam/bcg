import { Action } from "@ngrx/store";
import { polls } from '../reducers/category.reducer';
import { survey } from '../reducers/survey.reducer';
import { appurl } from '../reducers/appurl.reducer';

export enum ActionTypes {
    Polls_CategoryData="[Data] polls data",
    SurveysData="[Data] surveys data",
    AppUrlData="[Data] appurl data",
    removeData="Delete Data",
  }
  
  
  export class Polls_CategoryData implements Action{
    readonly type = ActionTypes.Polls_CategoryData;

    constructor(public payload:polls){}
  }

  export class SurveysData implements Action{
    readonly type = ActionTypes.SurveysData;

    constructor(public payload:survey){}
  }

  export class AppUrlData implements Action{
    readonly type = ActionTypes.AppUrlData;

    constructor(public payload:appurl){}
  }

  export class removeData implements Action{
    readonly type = ActionTypes.removeData;

    constructor(public unique_id:number){}
  }
  
  export type ActionsUnion =Polls_CategoryData|removeData|SurveysData|AppUrlData;
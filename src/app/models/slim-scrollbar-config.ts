/**
 * Project: life BCG
 * File: slim-scrollbar-config
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */


export class SlimScrollBarConfig {
    public static scrollBarConfig = {
        alwaysVisible: true,
        barOpacity: '0.5',
        gridBackground: 'transparent',
        barBackground: '#3fad93',
        barWidth: '4',
    };
}
/**
 * Project: life BCG
 * File: index
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

export * from './create-category';
export * from './slim-scrollbar-config';
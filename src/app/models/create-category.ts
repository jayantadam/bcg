/*
 * Project: life BCG
 * File: create-category
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */


export interface Offices {
    data: OfficeEntity[];
}

export interface OfficeEntity {
  address: string;
  name: string; 
  office_id: string;
  updated_by: string;
  created_by: string;
}

export interface FunctionCohort {
  data: FunctionCohortEntity[];
}

export interface FunctionCohortEntity {
  function_cohort_id: string;
  name: string; 
  description: string;
  updated_by: string;
  created_by: string;
}


export interface Group {
  data: GroupEntity[];
}

export interface GroupEntity {
  group_id: string;
  name: string; 
  description: string;
  updated_by: string;
  created_by: string;
}

export interface Category {
  data: CategoryEntity[];
}

export interface CategoryEntity {
  name: string; 
}

export interface User {
  data: UserEntity[];
}

export interface UserEntity {
  user_id: string; 
  name: string;
}


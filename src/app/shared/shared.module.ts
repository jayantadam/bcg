/**
 * Project: life BCG
 * File: shared.module
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NotificationSubMenuComponent } from '@shared-components/notification-sub-menu/notification-sub-menu.component';
import { TimeDifferencePipe } from './pipes/time-difference.pipe';
import { RadioComponent } from './components/radio/radio.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LazyloadDirective } from './directives/lazyload.directive';

import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';
import { ButtonComponent } from './components/button/button.component';
import { SelectComponent } from './components/select/select.component';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { SortByPipe } from './pipes/sort-by.pipe';
import { FilterByPipe } from './pipes/filter-by.pipe';


import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { DragDropPopoverComponent } from './components/drag-drop-popover/drag-drop-popover.component';

import { InputFieldComponent } from './components/input-field/input-field/input-field.component';

import { CarouselModule } from 'ngx-owl-carousel-o';
import { DragNDropInputComponent } from './components/drag-n-drop-input/drag-n-drop-input.component';
import { CalendarPopoverComponent } from './components/calendar-popover/calendar-popover.component';

import { CkeditorsComponent } from './components/ckeditors/ckeditors.component';


import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
@NgModule({

  declarations: [NotificationSubMenuComponent, TimeDifferencePipe, RadioComponent, LazyloadDirective, ButtonComponent, SelectComponent, SortByPipe, FilterByPipe, DragDropPopoverComponent, DragNDropInputComponent, CalendarPopoverComponent, CkeditorsComponent,InputFieldComponent],

  imports: [
    CommonModule,
    RouterModule,
    NgxSpinnerModule,
    NgSlimScrollModule,
    ReactiveFormsModule,
    BsDropdownModule.forRoot(),
    PopoverModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    TabsModule.forRoot(),
    CarouselModule,
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule
  ],
  exports: [
    NotificationSubMenuComponent,
    TimeDifferencePipe,
    RadioComponent,
    NgxSpinnerModule,
    LazyloadDirective,
    NgSlimScrollModule,
    ButtonComponent,
    SelectComponent,
    PopoverModule,
    SortByPipe,
    FilterByPipe,
    BsDatepickerModule,
    TimepickerModule,
    TabsModule,
    DragDropPopoverComponent,

    InputFieldComponent,
    CarouselModule,
    DragNDropInputComponent,
    CalendarPopoverComponent,
    ReactiveFormsModule,
    CkeditorsComponent
  ],
  providers: [{
    provide: SLIMSCROLL_DEFAULTS,
    useValue: {
      alwaysVisible: true,
      barOpacity: '0.5',
      gridBackground: 'transparent',
      barBackground: '#3fad93',
      barWidth: '4',
    }
  }]
})
export class SharedModule { }

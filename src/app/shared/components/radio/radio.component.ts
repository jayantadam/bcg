/**
 * Project: life BCG
 * File: radio.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})
export class RadioComponent {
  @Input() label?: string;
  @Input() control;
  @Input() disabled?: boolean;
  @Input() name?: string;
  @Input() value?: string;
  @Input() data;
  @Input() last?:string;
  @Input() percentage?:string;
  @Input() type?:string;
  @Input() checked;
  @Input() id;
  
  @Output() change = new EventEmitter<void>();

  constructor() {
   }

  onChange = (event) => {
    this.change.emit(event);
  };

}

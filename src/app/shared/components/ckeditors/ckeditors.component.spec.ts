import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CkeditorsComponent } from './ckeditors.component';

describe('CkeditorsComponent', () => {
  let component: CkeditorsComponent;
  let fixture: ComponentFixture<CkeditorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CkeditorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CkeditorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

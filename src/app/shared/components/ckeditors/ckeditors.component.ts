import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

@Component({
  selector: 'app-ckeditors',
  templateUrl: './ckeditors.component.html',
  styleUrls: ['./ckeditors.component.scss']
})
export class CkeditorsComponent implements OnInit {

  @Output() onChange = new EventEmitter<any>()
  ckEditor = ClassicEditor;

  config = {
    toolbarLocation : 'bottom',
    toolbar: [
      'heading',
      '|',
      'bold',
      'italic',
      'link',
      'bulletedList',
      'numberedList',
      '|',
      'Outdent',
      'Indent',
      '|',
      'blockQuote',
      '|',
      'undo',
      'redo'
    ],
    placeholder: 'Type your content here.'
  };

  constructor() { }

  ngOnInit(): void {
  }

  onEditorChange( { editor } : ChangeEvent ) {
    const data = editor.getData();
    console.log( data );
    this.onChange.emit(data);
  }

}

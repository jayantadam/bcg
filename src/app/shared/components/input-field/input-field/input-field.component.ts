import { Component, OnInit,Input,EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss']
})
export class InputFieldComponent implements OnInit {

  @Input() data;
  @Output() change = new EventEmitter<void>();
  constructor() { 
  }

  ngOnInit(): void {
  }

  OnChange=(event)=>{
    this.change.emit(event);
    console.log(event,"event s");
    
  }
}

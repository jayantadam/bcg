/**
 * Project: life BCG
 * File: select.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { throttleTime } from 'rxjs/operators';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

  @Input() label?: string;
  @Input() disabled?: boolean;
  @Input() error?: string;
  @Input() isRequired = false;
  @Input() throttleTime = 500;
  @Input() optionData?: [] = [];
  @Input() dropdownWidthClass?: string;
  private changeEventSource = new EventEmitter<void>();

  @Output() onChange = this.changeEventSource.pipe(throttleTime(this.throttleTime));

  @Input() selectedText = [{
    name : 'Select',
    value : true
  }];

  constructor() { }

  ngOnInit(): void {
  }

  setSelectedValue(value) {
    this.selectedText[0].name = value.name;
    this.changeEventSource.emit(value);
  }

}

/**
 * Project: life BCG
 * File: button.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { throttleTime } from 'rxjs/operators';
import { SpinnerService } from '@services/spinner.service';


@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

  @Input() submit?: boolean;
  @Input() classes;
  @Input() disabled = false;
  @Input() loading = false;
  @Input() throttleTime = 500;

  private clickEventSource = new EventEmitter<void>();
  
  @Output() onClick = this.clickEventSource.pipe(throttleTime(this.throttleTime));

  constructor(
    private spinnerService: SpinnerService
  ) { 
    
  }

  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    
    if(this.loading == true) {
      this.spinnerService.showSpinner();
    }
  }

  get type() {
    return this.submit ? 'submit' : 'button';
  }

  onClickBtn() {
    this.clickEventSource.emit();
  };
}


import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { throttleTime, map, expand } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppUrlData } from '@ngrx/category/actions/category.action';
@Component({
  selector: 'app-drag-drop-popover',
  templateUrl: './drag-drop-popover.component.html',
  styleUrls: ['./drag-drop-popover.component.scss']
})
export class DragDropPopoverComponent implements OnInit {

  @Input() pop;
  @Input() module;
  @Input() title;
  @Input() message;
  @Input() throttleTime = 500;

  imageFile = [];
  fileName: string;
  private clickEventSource = new EventEmitter<any>();

  @Output() onUpdate = this.clickEventSource.pipe(throttleTime(this.throttleTime));

  drag_drop_form: FormGroup;
  submitted = false;
  multipleFlag = false;
  acceptType = "image/*";
  uploadType = '';
  appUrlFileData = [];
  appUrlData;
  constructor(
    private ref: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private store: Store,
  ) { }

  ngOnInit(): void {
    if (this.module == 'uploadAppUrl') {
      this.uploadType = "notImage";
      this.multipleFlag = true;
      this.acceptType = "application/pdf,application/vnd.ms-excel"
    }
    this.drag_drop_form = this.formBuilder.group({
      appName: ['', Validators.required],
      appDescription: ['', Validators.required],
      materialFiles: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.drag_drop_form.controls; }

  onSelect(event) {
    this.ref.detectChanges();

    if (this.module == 'uploadAppUrl') {
      console.log("files =", event)
      this.appUrlFileData.push(...event.addedFiles);
      this.drag_drop_form.patchValue({ materialFiles: this.appUrlFileData })
    } else {
      this.imageFile = event.addedFiles;
      this.fileName = event.addedFiles[0].name;
    }

    this.ref.detectChanges();
  }

  uploadMediaContent = () => {

    if (this.module == 'uploadAppUrl') {
      this.submitted = true;

      if (this.drag_drop_form.valid) {

        this.clickEventSource.emit(
          {
            module: this.module,
            title: this.drag_drop_form.value.appName,
            files: this.drag_drop_form.value.materialFiles,
            description: this.drag_drop_form.value.appDescription
          });

          this.appUrlData = {
            appUrlData: {
              module: this.module,
              title: this.drag_drop_form.value.appName,
              files: this.drag_drop_form.value.materialFiles,
              description: this.drag_drop_form.value.appDescription,
              unique_id: Math.random(),
              visible: true
            },
          };

          this.store.dispatch(new AppUrlData(this.appUrlData));
          

      } else {
        return;
      }

    } else {
      this.clickEventSource.emit({ module: this.module, image: this.imageFile });
    }
    this.pop.hide();

  }

}

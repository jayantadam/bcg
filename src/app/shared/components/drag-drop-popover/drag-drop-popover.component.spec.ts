import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragDropPopoverComponent } from './drag-drop-popover.component';

describe('DragDropPopoverComponent', () => {
  let component: DragDropPopoverComponent;
  let fixture: ComponentFixture<DragDropPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragDropPopoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragDropPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

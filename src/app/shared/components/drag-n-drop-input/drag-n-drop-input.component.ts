import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { throttleTime, map, expand } from 'rxjs/operators';
import { DragNDropService } from "@services/drag-n-drop.service";
import { coerceBooleanProperty, coerceNumberProperty } from '@helpers/type-helper';
import { EMPTY } from 'rxjs';
import { GalleryImagesService } from "@services/gallery-images.service";
import { CreateCategoryService } from '@services/create-category.service';

export interface NgxDropzoneChangeEvent {
  source: DragNDropInputComponent;
  addedFiles: File[];
  rejectedFiles: File[];
}

export interface CompressDropzoneChangeEvent {
  addedFiles: File[];
}

@Component({
  selector: 'app-drag-n-drop-input',
  templateUrl: './drag-n-drop-input.component.html',
  styleUrls: ['./drag-n-drop-input.component.scss']
})
export class DragNDropInputComponent implements OnInit {

  uploadFile: string;
  @Input() accept: string = '*';
  @Input() throttleTime = 500;
  @Input() id;
  @Input() classes = "custom-file-input";
  @Input() labelClass = "custom-file-label box-shadow";
  @Input() noClass: boolean;
  @Input() uploadTypes : string;

  /** A template reference to the native file input element. */
  @ViewChild('fileInput') _fileInput: ElementRef;

  /** Allow the selection of multiple files. */
  @Input()
  get multiple(): boolean {
    return this._multiple;
  }
  set multiple(value: boolean) {
    this._multiple = coerceBooleanProperty(value);
  }
  private _multiple = true;


  /** Set the maximum size a single file may have. */
  @Input()
  get maxFileSize(): number {
    return this._maxFileSize;
  }
  set maxFileSize(value: number) {
    this._maxFileSize = coerceNumberProperty(value);
  }
  private _maxFileSize: number = undefined;


  // private changeEventSource = new EventEmitter<void>();

  // @Output() onChange = this.changeEventSource.pipe(throttleTime(this.throttleTime));

  // @Output() readonly onChange = new EventEmitter<NgxDropzoneChangeEvent>();
  @Output() readonly onChange = new EventEmitter<any>();
  


  constructor(
    private dragNDropService: DragNDropService,
    private galleryImagesService: GalleryImagesService,
    private createCategoryService: CreateCategoryService
  ) { }

  ngOnInit(): void {
  }

  // onFilesSelected(event) {
  //   var files = event.target.files;
  //   this.handleFileDrop(files);

  //   this._fileInput.nativeElement.value = '';

  //   this.preventDefault(event);
  // }

  // private handleFileDrop(files) {
  //   const result = this.dragNDropService.parseFileList(files, this.accept, this.maxFileSize, this.multiple);

  //   this.onChange.next({
  //     addedFiles: result.addedFiles,
  //     rejectedFiles: result.rejectedFiles,
  //     source: this
  //   });
  // }



  data: FileList;
  compressedImages = [];
  recursiveCompress = (image: File, index, array) => {
    return this.dragNDropService.compress(image, 75, this.accept, this.maxFileSize).pipe(
      map(response => {

        //Code block after completing each compression
        // console.log('compressed ' + index + image.name);
        if (response)
          this.compressedImages.push(response);

        return {
          data: response,
          index: index + 1,
          array: array,
        };
      }),
    );
  }

  //process files for upload
  public process(event) {
    this.compressedImages = [];
    this.data = event.target.files;
    
    if(this.uploadTypes == 'notImage') {
      this.onChange.next({
        addedFiles: this.data,
      });
    }else {
    
    const compress = this.recursiveCompress(this.data[0], 0, this.data).pipe(
      expand(res => {
        return res.index > res.array.length - 1
          ? EMPTY
          : this.recursiveCompress(this.data[res.index], res.index, this.data);
      }),
    );
    compress.subscribe(res => {
      if (res.index > res.array.length - 1) {

        //Code block after completing all compression
        // console.log('Compression successful ', this.compressedImages);
        // this.galleryImagesService.setGalleryData(this.compressedImages)
        this._fileInput.nativeElement.value = '';
        this.onChange.next({
          addedFiles: this.compressedImages,
        });
        
      }
    });
    // this._fileInput.nativeElement.value = '';
    this.preventDefault(event);
  }
  }





  private preventDefault(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();
  }


}

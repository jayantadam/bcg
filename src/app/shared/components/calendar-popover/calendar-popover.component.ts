import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { throttleTime } from 'rxjs/operators';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import * as moment from 'moment-timezone';
import { FormControl, AbstractControl } from '@angular/forms';
@Component({
  selector: 'app-calendar-popover',
  templateUrl: './calendar-popover.component.html',
  styleUrls: ['./calendar-popover.component.scss']
})
export class CalendarPopoverComponent implements OnInit {

  bsInlineValue = new Date();

  @Input() pop;
  @Input() title;
  @Input() module;
  @Input() throttleTime = 500;

  eventDateObj = {
    start_date: null,
    end_date: null,
    type: null
  };

  eventTimeObj = {
    start_time: null,
    end_time: null,
  };

  calendarType = [
    {
      name: 'Type1'
    },
    {
      name: 'Type2'
    }
  ]

  endDateFlag: boolean = false;
  endTimeFlag: boolean = false;
  minDate: Date;
  myTime: Date = new Date();
  ctrl;
  bsConfig: Partial<BsDatepickerConfig>;

  private clickEventSource = new EventEmitter<any>();

  @Output() onAdd = this.clickEventSource.pipe(throttleTime(this.throttleTime));


  constructor() {
    this.bsConfig = Object.assign({}, { containerClass: 'theme-blue' });
    this.minDate = new Date();

    this.ctrl = new FormControl('', (control: AbstractControl) => {
      const value = control.value;

      if (!value) {
        return null;
      }

      const hours = value.getHours();
      console.log("hours = ", hours);

      if (hours > 24) {
        return { outOfRange: true };
      }

      return null;
    });


  }

  ngOnInit(): void {
    this.eventTimeObj.start_time = moment(this.myTime).format('HH:mm')
  }

  onChange = (event) => {
    if (event) {
      this.endDateFlag = true;
      this.minDate.setDate(moment(this.eventDateObj.start_date).format('DD'));
    } else {
      this.endDateFlag = false;
      this.minDate.setDate(moment().format('DD'));
      this.eventDateObj.end_date = null;
    }

  }

  onValueChange = (date) => {
    console.log("moment =", moment(date).format('YYYY-MM-DD'))
    // console.log("date =",date);

    console.log("endDateFlag = ", this.endDateFlag)
    if (this.endDateFlag) {
      this.eventDateObj.end_date = moment(date).format('YYYY-MM-DD')
    } else {
      this.eventDateObj.start_date = moment(date).format('YYYY-MM-DD')
    }

    console.log("date =", this.eventDateObj);


  }

  getSelectedCalendarType = (event) => {
    console.log("event =", event);
    this.eventDateObj.type = event.name;
  }
  setDateValue = () => {

    console.log("date =", this.eventDateObj);
    this.clickEventSource.emit(this.eventDateObj);
    this.pop.hide();
  }

  onChangeEndTime = (event) => {
    if (event) {
      this.endTimeFlag = true;
      // this.minDate.setDate(moment(this.eventDateObj.start_date).format('DD'));
    } else {
      this.endTimeFlag = false;
      // this.minDate.setDate(moment().format('DD'));
      this.eventTimeObj.end_time = null;
    }

  }

  setTimeValue = () => {

    if (this.ctrl.status == 'INVALID' || moment(this.myTime).format('HH:mm') == 'Invalid date') {
      return
    } else {
      if (this.endTimeFlag) {
        this.eventTimeObj.end_time = moment(this.myTime).format('HH:mm')
      } else {
        this.eventTimeObj.start_time = moment(this.myTime).format('HH:mm')
      }
      console.log('eventTimeObj = ', this.eventTimeObj)

      this.clickEventSource.emit(this.eventTimeObj);
      this.pop.hide();
      
    }
  }
}

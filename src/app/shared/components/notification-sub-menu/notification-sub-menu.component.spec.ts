import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationSubMenuComponent } from './notification-sub-menu.component';

describe('NotificationSubMenuComponent', () => {
  let component: NotificationSubMenuComponent;
  let fixture: ComponentFixture<NotificationSubMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationSubMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationSubMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

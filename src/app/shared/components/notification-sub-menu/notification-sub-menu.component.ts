/**
 * Project: life BCG
 * File: notification-sub-menu.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-notification-sub-menu',
  templateUrl: './notification-sub-menu.component.html',
  styleUrls: ['./notification-sub-menu.component.scss']
})
export class NotificationSubMenuComponent implements OnInit {

  @Input() data;
  constructor() { }

  ngOnInit(): void {
  }

}

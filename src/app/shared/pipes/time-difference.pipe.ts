/**
 * Project: life BCG
 * File: time-difference.pipe
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

/*
 *{{ isNgTemplate.date | timeDifference }}
*/

import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment-timezone';

@Pipe({
  name: 'timeDifference'
})
export class TimeDifferencePipe implements PipeTransform {

  transform(date: string | number): string | number {
    if (date) {
      return moment(date).fromNow(); 
    }
  }

}

/**
 * Project: life BCG
 * File: create-category.module
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryComponent } from './modal/category/category.component';
import { CategoryViewComponent } from './components/category-view/category-view.component';
import { SharedModule } from "../../shared/shared.module";
import { PocPopoverComponent } from './components/poc-popover/poc-popover.component';
import { PocComponent } from './components/poc/poc.component';
import { OfficeCohortGroupComponent } from './components/office-cohort-group/office-cohort-group.component';
import { OfficeCohortGroupPopoverComponent } from './components/office-cohort-group-popover/office-cohort-group-popover.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddGalleryComponent } from './components/more-section/add-gallery/add-gallery.component';
import { AddPollsComponent } from './components/more-section/add-polls/add-polls.component';
import { AddLogisticsComponent } from './components/more-section/add-logistics/add-logistics.component';
import { MainImageIconComponent } from './components/main-image-icon/main-image-icon.component';
import { CreateThreadComponent } from './components/create-thread/create-thread.component';
import { AddSurveysComponent } from './components/more-section/add-surveys/add-surveys.component';
import { AddAgendaComponent } from './components/more-section/add-agenda/add-agenda.component';
import { AddAppUrlPopoverComponent } from './components/add-app-url-popover/add-app-url-popover.component';

import { PollsPreviewComponent } from './components/more-section/polls-preview/polls-preview.component';

import { GalleryPreviewComponent } from './components/more-section/gallery-preview/gallery-preview.component';
import { PreviewImageComponent } from './components/more-section/preview-image/preview-image.component';




@NgModule({
  declarations: [CategoryComponent, CategoryViewComponent, PocPopoverComponent, PocComponent, OfficeCohortGroupComponent, OfficeCohortGroupPopoverComponent, AddGalleryComponent, AddPollsComponent, AddLogisticsComponent, MainImageIconComponent, CreateThreadComponent, AddSurveysComponent, AddAgendaComponent, AddAppUrlPopoverComponent, GalleryPreviewComponent, PreviewImageComponent,PollsPreviewComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports:[CategoryComponent],
})
export class CreateCategoryModule { }

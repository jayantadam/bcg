import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAppUrlPopoverComponent } from './add-app-url-popover.component';

describe('AddAppUrlPopoverComponent', () => {
  let component: AddAppUrlPopoverComponent;
  let fixture: ComponentFixture<AddAppUrlPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAppUrlPopoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAppUrlPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

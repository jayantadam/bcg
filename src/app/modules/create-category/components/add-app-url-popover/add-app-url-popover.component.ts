import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { throttleTime } from 'rxjs/operators';

@Component({
  selector: 'app-add-app-url-popover',
  templateUrl: './add-app-url-popover.component.html',
  styleUrls: ['./add-app-url-popover.component.scss']
})
export class AddAppUrlPopoverComponent implements OnInit {

  @Input() pop;
  @Input() title;
  @Input() throttleTime = 500;
  private clickEventSource = new EventEmitter<void>();
  
  @Output() onAdd = this.clickEventSource.pipe(throttleTime(this.throttleTime));
  
  constructor() { }

  ngOnInit(): void {
  }

}

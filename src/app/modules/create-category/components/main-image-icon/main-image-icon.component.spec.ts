import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainImageIconComponent } from './main-image-icon.component';

describe('MainImageIconComponent', () => {
  let component: MainImageIconComponent;
  let fixture: ComponentFixture<MainImageIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainImageIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainImageIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/**
 * Project: life BCG
 * File: main-image-icon.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input, EventEmitter, Output, SimpleChanges, OnChanges } from '@angular/core';
import { throttleTime } from 'rxjs/operators';
import { DragNDropService } from "@services/drag-n-drop.service";

@Component({
  selector: 'app-main-image-icon',
  templateUrl: './main-image-icon.component.html',
  styleUrls: ['./main-image-icon.component.scss']
})
export class MainImageIconComponent implements OnInit, OnChanges {

  @Input() module;
  @Input() title;
  @Input() file;
  @Input() throttleTime = 500;

  imageSrc: string | ArrayBuffer = '';

  private clickEventSource = new EventEmitter<void>();
  
  @Output() onDelete = this.clickEventSource.pipe(throttleTime(this.throttleTime));
  
  constructor(
    private dragNDropService: DragNDropService,
  ) { }

  ngOnInit(): void {
    this.dragNDropService.readFile(this.file)
    .then(img => setTimeout(() => { 
      this.imageSrc = img;
    }))
    .catch(err => console.error(err));
  }

  ngOnChanges(changes: SimpleChanges): void {

    let chng = changes['file'];
    let cur  = chng.currentValue;
    this.dragNDropService.readFile(cur)
    .then(img => setTimeout(() => { 
      this.imageSrc = img;
    }))
    .catch(err => console.error(err));
  }

  deleteMedia = () => {
    this.clickEventSource.emit()
  }



 

}

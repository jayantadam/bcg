import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeCohortGroupPopoverComponent } from './office-cohort-group-popover.component';

describe('OfficeCohortGroupPopoverComponent', () => {
  let component: OfficeCohortGroupPopoverComponent;
  let fixture: ComponentFixture<OfficeCohortGroupPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfficeCohortGroupPopoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficeCohortGroupPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

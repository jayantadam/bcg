/**
 * Project: life BCG
 * File: office-cohort-group-popover.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input, Output, EventEmitter, ɵConsole } from '@angular/core';
import * as moment from 'moment-timezone';
import { SlimScrollBarConfig, Offices } from "@models/index";
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { LoggerService } from '@services/logger.service';

@Component({
  selector: 'app-office-cohort-group-popover',
  templateUrl: './office-cohort-group-popover.component.html',
  styleUrls: ['./office-cohort-group-popover.component.scss']
})
export class OfficeCohortGroupPopoverComponent implements OnInit {

  officeCohortGroupForm: FormGroup;
  submitted = false;
  @Input() pop;
  @Input() popTitle: string;
  @Input() data;

  Name: any;

  @Output() updateData = new EventEmitter<void>();

  emailFormArray = [];
  slimScrollOpts = SlimScrollBarConfig.scrollBarConfig;
  constructor(
    private formBuilder: FormBuilder,
    private loggerService: LoggerService
  ) {


  }

  ngOnInit(): void {

    let allSelectedResult = this.data.map(val => val.isActive).every(Boolean);

    const selectAllControl = (allSelectedResult) ? new FormControl(true) : new FormControl(false);

    const formControls = this.data.map(control => {
      if (control.isActive)
        return new FormControl(true)
      else
        return new FormControl(false)
    });

    this.officeCohortGroupForm = this.formBuilder.group({
      officeCohortGroupText: [''],
      selectOfficeCohortGroupAll: selectAllControl,
      data: new FormArray(formControls),
      citySelectedFlag: [true, Validators.requiredTrue]
    });

    this.onSelectionChanges();
  }

  popoverClose() {
    this.pop.hide()
  }

  onChange(index, isChecked: boolean) {

    this.data = this.data.map((v,i) => {
      if (isChecked) {
        return i == index ? { ...v, isActive: true, checkedTime: moment().format() } : v
      } else {
        return i == index ? { ...v, isActive: false, checkedTime: '00:00' } : v
      }
    })
  }

  onSelectionChanges(): void {

    // Subscribe to changes on the selectAll checkbox
    this.officeCohortGroupForm.get('selectOfficeCohortGroupAll').valueChanges.subscribe(bool => {

      if (bool == false) {
        this.data = this.data.map(v => ({ ...v, isActive: false }));
        this.officeCohortGroupForm.patchValue({ citySelectedFlag: false })
      } else {
        this.data = this.data.map(v => ({ ...v, isActive: true }));
        this.officeCohortGroupForm.patchValue({ citySelectedFlag: true });
      }
      
      this.officeCohortGroupForm
        .get('data')
        .patchValue(Array(this.data.length).fill(bool), { emitEvent: false });
    });

    // Subscribe to changes on the city checkboxes
    this.officeCohortGroupForm.get('data').valueChanges.subscribe(val => {

      const allSelected = val.every(bool => bool);
      const notSelected = val.every(bool => !bool);

      if ((!allSelected && !notSelected) || allSelected)
        this.officeCohortGroupForm.patchValue({ citySelectedFlag: true });


      if (this.officeCohortGroupForm.get('selectOfficeCohortGroupAll').value !== allSelected) {
        this.officeCohortGroupForm.get('selectOfficeCohortGroupAll').patchValue(allSelected, { emitEvent: false });
      }

      if (notSelected == true) {
        this.officeCohortGroupForm.patchValue({ citySelectedFlag: false });
      }

      // this.data = this.officeCohortGroupForm.value.data.map((checked, index) => {
      //   if (checked) {
      //     return { ...this.data[index], isActive: true, checkedTime: moment().format() }
      //   } else {
      //     return { ...this.data[index], isActive: false, checkedTime: '00:00' }
      //   }
      // })

    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.officeCohortGroupForm.controls; }


  onSubmitOfficeCohortGroup() {
    this.submitted = true;

    if (this.officeCohortGroupForm.valid) {

      this.updateData.emit(this.data)
      this.pop.hide();

    } else {
      return;
    }
  }

}

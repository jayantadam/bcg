/**
 * Project: life BCG
 * File: gallery-preview.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { OwlOptions, SlidesOutputData } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-gallery-preview',
  templateUrl: './gallery-preview.component.html',
  styleUrls: ['./gallery-preview.component.scss']
})
export class GalleryPreviewComponent implements OnInit {

  customOptions: OwlOptions = {
    items: 4,
    margin: 10,
    loop: false,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    dragEndSpeed:1000,
    skip_validateItems: true,
    nav: true
  }

  @Input() files;
  @Output() updatedFiles = new EventEmitter<any>();
  constructor(
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    // console.log("files = ",this.files);
  }

  onSelect(event) {
    
    this.ref.detectChanges();
    this.files.push(...event.addedFiles);
    
    setTimeout(() => {
      console.log("settimeout")
      this.ref.detectChanges();
    }, 1000);

    this.ref.detectChanges();
  }

  getUpdatedBase64Data(event) {
    console.log("getUpdatedBase64Data = ",event);
  }
  
  getData(data: SlidesOutputData) {
    setTimeout(() => {
      this.ref.detectChanges();
    });
  }

  

}

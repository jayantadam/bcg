/**
 * Project: life BCG
 * File: add-polls.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import {Component,OnInit,Input,Output,EventEmitter,ChangeDetectorRef} from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { Observable, from } from 'rxjs';

import {Polls_CategoryData} from '../../../../../core/ngrxstore/category/actions/category.action';
import { polls } from '../../../../../core/ngrxstore/category/reducers/category.reducer';


import {ToastsService} from'@services/toasts.service';

@Component({
  selector: 'app-add-polls',
  templateUrl: './add-polls.component.html',
  styleUrls: ['./add-polls.component.scss'],
})
export class AddPollsComponent implements OnInit {
  pollsquestion$: Observable<polls>;
  questionForm: FormGroup;
  isSubmitted = false;
  addAgendaFlag=false;
  questionData;
  previewData=[];
  // @Input() formData;
  pollQuestionData = [
    {
      name: 'Enter your question here',
      formControll: 'question',
      validation: false,
    },
    {
      name: 'Answer 01',
      formControll: 'answer_1',
      validation: false,
    },
    {
      name: 'Answer 02',
      formControll: 'answer_2',
      validation: false,
    },
  ];

  question = {
    name: 'Enter your question here',
    formControll: 'question',
  };
  addAnswerFlag = false;
  @Output() closeContent = new EventEmitter<void>();
  constructor(
    private formBuilder: FormBuilder,
    private store: Store<{ polls: polls }>,
    private toast:ToastsService
  ) {
    // console.log(this.formData,"form data");

    this.questionForm = this.formBuilder.group({
      question: new FormControl(
        '',
        Validators.compose([
          //fetch value from userid field and validate it
          Validators.required,
        ])
      ),
      answer_1: new FormControl(
        '',
        Validators.compose([
          //fetch value from password feild and validate it
          Validators.required,
        ])
      ),
      answer_2: new FormControl(
        '',
        Validators.compose([
          //fetch value from password feild and validate it
          Validators.required,
        ])
      ),
    });

    this.pollsquestion$ = store.pipe(select('polls'));
  }

  ngOnInit(): void {}

  closeContentVew = () => {
    this.closeContent.emit();
  };

  addAnswer = () => {
    let c = 'answer_0' + (this.pollQuestionData.length + 1);
    if (this.pollQuestionData.length < 6) {
      this.pollQuestionData.push({
        name: 'Answer 0' + (this.pollQuestionData.length + 1),
        formControll: 'answer_0' + (this.pollQuestionData.length + 1),
        validation: false,
      });
    } else {
      this.addAnswerFlag = true;
    }

    this.questionForm.addControl(
      c,
      new FormControl(
        '',
        Validators.compose([
          //fetch value from password feild and validate it
          Validators.required,
        ])
      )
    );
  };

  get f() {
    console.log(this.questionForm.controls, 'f function');
    return this.questionForm.controls;
  }

  async saveData() {
    const invalid = [];
    const controls = this.questionForm.controls;
    let counter = null;
    for (const name in controls) {
      counter = counter == null ? 0 : counter + 1;

      if (controls[name].invalid) {
        this.pollQuestionData[counter].validation = true;
        invalid.push(name);
      } else {
        this.pollQuestionData[counter].validation = false;
      }
    }

    if (invalid.length == 0) {
      let temp: object[] = [];

      let answers =Object.values(this.questionForm.value);

      await this.pollsquestion$.subscribe((data) => {
        temp = data.pollsData;
      });
      this.questionData = {
        pollsData: {
          question: this.questionForm.get('question').value,
          answers:answers,
          type: 'pollscategory',
          unique_id: Math.random(),
          visible: true
        },
      };
      this.store.dispatch(new Polls_CategoryData(this.questionData));
      this.toast.showSuccess('Polls inserted successfully');
      this.questionForm.reset();
      //setTimeout(() => {
        this.pollsquestion$.subscribe((data) => {
          console.log(data.pollsData, 'prints');
          this.previewData = data.pollsData;
          this.addAgendaFlag=true;        
        });
      //}, 1000);
    } else {
      return false;
    }
  }
}

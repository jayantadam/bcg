import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-agenda',
  templateUrl: './add-agenda.component.html',
  styleUrls: ['./add-agenda.component.scss']
})
export class AddAgendaComponent implements OnInit {

  @Output() closeContent = new EventEmitter<void>();
  constructor() { }

  ngOnInit(): void {
  }

  closeContentVew = () => {
    this.closeContent.emit()
  }

  onEditorChange = (agendaData) => {
    console.log("Agenda = ",agendaData)
  }

}

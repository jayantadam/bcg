import { Component, OnInit,Input } from '@angular/core';
import { polls } from 'src/app/core/ngrxstore/category/reducers/category.reducer';
import { Store } from '@ngrx/store';
import { removeData } from 'src/app/core/ngrxstore/category/actions/category.action';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-polls-preview',
  templateUrl: './polls-preview.component.html',
  styleUrls: ['./polls-preview.component.scss']
})
export class PollsPreviewComponent implements OnInit {

  @Input() questionData;
  pollsquestion$: Observable<polls>;
  
  constructor(
    private store: Store<{ polls: polls }>
  ) {

    
   }

  ngOnInit(): void {
    console.log(this.questionData,"preview data");
    //this.questionData=this.previewData
  }

  deletePoll(unique_id){
    this.store.dispatch(new removeData(unique_id))
    // setTimeout(() => {
    //   this.pollsquestion$.subscribe((data) => {
    //     console.log(data.pollsData, 'preview remove data');     
    //   });
    // }, 3000);

  }
}

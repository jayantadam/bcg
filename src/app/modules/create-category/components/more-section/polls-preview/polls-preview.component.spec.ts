import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollsPreviewComponent } from './polls-preview.component';

describe('PollsPreviewComponent', () => {
  let component: PollsPreviewComponent;
  let fixture: ComponentFixture<PollsPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

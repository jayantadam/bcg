/**
 * Project: life BCG
 * File: add-logistics.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-logistics',
  templateUrl: './add-logistics.component.html',
  styleUrls: ['./add-logistics.component.scss']
})
export class AddLogisticsComponent implements OnInit {

  @Output() closeContent = new EventEmitter<void>();
  constructor() { }

  ngOnInit(): void {
  }

  closeContentVew = () => {
    this.closeContent.emit()
  }

}

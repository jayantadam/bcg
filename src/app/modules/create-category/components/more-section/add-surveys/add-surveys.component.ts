import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from '@angular/forms';
import { Store } from '@ngrx/store';
import { SurveysData } from 'src/app/core/ngrxstore/category/actions/category.action';
import { ToastsService } from '@services/toasts.service';

@Component({
  selector: 'app-add-surveys',
  templateUrl: './add-surveys.component.html',
  styleUrls: ['./add-surveys.component.scss']
})
export class AddSurveysComponent implements OnInit {

  @Output() closeContent = new EventEmitter<void>();
  surveyForm: FormGroup;
  surveyData;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private toast:ToastsService
  ) {
    this.surveyForm = this.formBuilder.group({
      url: new FormControl(
        '',
        Validators.compose([
          //fetch value from url field and validate it
          Validators.required,
          Validators.pattern("^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$")
        ])
      ),
      description: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      )
    });
   }

  ngOnInit(): void {
  }
  get f() { return this.surveyForm.controls; }
  closeContentVew = () => {
    this.closeContent.emit()
  }
  saveData(){
    this.submitted = true;
    console.log(this.surveyForm.value,"servey value");
    // stop here if form is invalid
    if (this.surveyForm.valid) {
      this.surveyData = {
        surveyData: {
          name: "NA",
          url: this.surveyForm.get('url').value,
          description:this.surveyForm.get('description').value,
          type: 'surveycategory',
          unique_id: Math.random(),
          visible: true
        },
      };
      this.store.dispatch(new SurveysData(this.surveyData));
      this.toast.showSuccess('Survey inserted successfully');
      this.surveyForm.reset();
    }
    else{
      return false;
    }
    }
   
}

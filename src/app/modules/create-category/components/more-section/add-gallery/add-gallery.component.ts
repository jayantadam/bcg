/**
 * Project: life BCG
 * File: add-gallery.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Output, Input, EventEmitter, SimpleChanges, OnChanges, ChangeDetectorRef } from '@angular/core';

import { GalleryImagesService } from "@services/gallery-images.service";


@Component({
  selector: 'app-add-gallery',
  templateUrl: './add-gallery.component.html',
  styleUrls: ['./add-gallery.component.scss']
})
export class AddGalleryComponent implements OnInit {

  files = [];
  base64array = [];
  
  @Output() closeContent = new EventEmitter<void>();
  @Output() updatedFiles = new EventEmitter<any>();

  constructor(
    private galleryImagesService: GalleryImagesService,
    private ref: ChangeDetectorRef
  ) {
    // ref.detach();
    // setInterval(() => {
    //   this.ref.detectChanges();
    // }, 1000);
  }

  ngOnInit(): void {
  }

  closeContentVew = () => {
    this.closeContent.emit()
  }

  onSelect(event) {
    
    this.ref.detectChanges();
    
    this.files.push(...event.addedFiles);
    
    setTimeout(() => {
      this.ref.detectChanges();
    },1000);
    
    this.updatedFiles.emit(this.files);
    this.ref.detectChanges();
  }

}

/**
 * Project: life BCG
 * File: preview-image.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DragNDropService } from "@services/drag-n-drop.service";
@Component({
  selector: 'app-preview-image',
  templateUrl: './preview-image.component.html',
  styleUrls: ['./preview-image.component.scss']
})
export class PreviewImageComponent implements OnInit {

  base64array = []
  @Input() file;
  @Input() index;
  deleteImageArray = [];
  imageSrc: string | ArrayBuffer = '';
  constructor(
    private dragNDropService: DragNDropService
  ) { 
    console.log("ng on constructor")
  }

  ngOnInit() {

    console.log("ng on ot")
    this.dragNDropService.readFile(this.file)
      .then(img => setTimeout(() => { 
        this.imageSrc = img;
      }))
      .catch(err => console.error(err));

  }

  onImageSelect = (index,selectValue) => {
    console.log("onImageSelect",selectValue)
    console.log("this.deleteArray before",this.deleteImageArray)
    if(selectValue) {
      this.deleteImageArray.push(index)
    }else{
      console.log('inside else')
     // this.deleteImageArray.filter(e => e !== index)
    }

    console.log("this.deleteArray after",this.deleteImageArray)
  }

}

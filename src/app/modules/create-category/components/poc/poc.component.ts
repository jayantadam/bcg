/**
 * Project: life BCG
 * File: poc.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-poc',
  templateUrl: './poc.component.html',
  styleUrls: ['./poc.component.scss']
})
export class PocComponent implements OnInit {

  @Input() officesData;
  @Input() userData;
  @Output() getPocData = new EventEmitter<any>();
  empData = new Array();
  constructor() { }

  ngOnInit(): void {
  }

  popover: any;
  openPopover(popTemplate) {
    this.popover = popTemplate;
  }

  updateDataSet = (dataSet) => {
    this.empData = dataSet;
    this.getPocData.emit(this.empData.map(({name, ...keepAttrs}) => keepAttrs));
  }

  deleteEmp = (index) => { 
    this.empData.splice(index,1);
  }


}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-create-thread',
  templateUrl: './create-thread.component.html',
  styleUrls: ['./create-thread.component.scss']
})
export class CreateThreadComponent implements OnInit {

  @Output() closeContent = new EventEmitter<void>();
  constructor() { }

  ngOnInit(): void {
  }

  closeContentVew = () => {
    this.closeContent.emit()
  }

}

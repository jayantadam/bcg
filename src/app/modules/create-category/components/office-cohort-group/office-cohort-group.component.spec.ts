import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeCohortGroupComponent } from './office-cohort-group.component';

describe('OfficeCohortGroupComponent', () => {
  let component: OfficeCohortGroupComponent;
  let fixture: ComponentFixture<OfficeCohortGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfficeCohortGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficeCohortGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/**
 * Project: life BCG
 * File: office-cohort-group.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-office-cohort-group',
  templateUrl: './office-cohort-group.component.html',
  styleUrls: ['./office-cohort-group.component.scss']
})
export class OfficeCohortGroupComponent implements OnInit {

  @Input() title: string;
  @Input() popTitle?: string;
  @Input() data;
  @Output() getOfficeData = new EventEmitter<any>();
  @Output() getFunctionCohortsData = new EventEmitter<any>();
  @Output() getGroupData = new EventEmitter<any>();

  popover: any;

  constructor() { }

  ngOnInit(): void {
  }

  openPopover(popTemplate) {
    this.popover = popTemplate;
  }

  updateDataSet = (dataSet) => {

    console.log("data =====", dataSet)

    this.data = dataSet;
    if (this.title == 'Offices') {
      this.getOfficeData.emit(this.data.filter(val => val.isActive ).map(value => value.office_id))
    }

    if (this.title == 'Functions/Cohorts'){
      this.getFunctionCohortsData.emit(
        this.data.filter(val => val.isActive == true).map(res => {
          const container = {};
          container['function_id'] = res.function_id;
          container['type'] = res.type;
          return container;
        })
        )
      }

    if (this.title == 'Groups') {
      this.getGroupData.emit(this.data.filter(val => val.isActive ).map(value => value.group_id))
    }
  }

  removeItemData(data) {
    this.data = this.data.map(v => {
      return v.name == data.name ? { ...v, isActive: false } : v
    })

    this.updateDataSet(this.data)
    console.log("this.data = ",this.data)
    this.popover.hide();
  }

}

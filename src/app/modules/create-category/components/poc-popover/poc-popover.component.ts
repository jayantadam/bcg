/**
 * Project: life BCG
 * File: poc-popover.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SlimScrollBarConfig } from 'src/app/models/slim-scrollbar-config';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';


@Component({
  selector: 'app-poc-popover',
  templateUrl: './poc-popover.component.html',
  styleUrls: ['./poc-popover.component.scss']
})
export class PocPopoverComponent implements OnInit {

  @Input() pop;
  @Input() selectedEmp = new Array();
  @Input() userData = new Array();
  @Input() officesData = new Array();
  @Output() updateData = new EventEmitter<any>();

  pocForm: FormGroup;
  submitted = false;
  showMoreDetails: boolean = false;
  selectedItem: any;

  selectedEmpDetails = {
    name : null,
    user_id : null,
    user_type : null
  }

  selectedEmpData = new Array();
  found: boolean;
  errorMesg: string = '';
  slimScrollOpts = SlimScrollBarConfig.scrollBarConfig;


  pocType = new FormControl('', [Validators.required]);

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    // const formControls = this.cityData.map(control => new FormControl(false));

    const selectAllControl = new FormControl(true);

    this.pocForm = this.formBuilder.group({
      NonBcgEmpName: [''],
      BcgEmpName: [''],
      cityName: [''],
      pocType: this.pocType,
      pocDescription: [''],
      selectAll: selectAllControl,
      citySelectedFlag: [true, Validators.requiredTrue]
    });

    const formControls = this.officesData.map(control => new FormControl(true));
    this.pocForm.addControl('officesData', new FormArray(formControls))
    this.onChanges();

  }

  onChanges(): void {
    // Subscribe to changes on the selectAll checkbox
    this.pocForm.get('selectAll').valueChanges.subscribe(bool => {

      (bool == false) ? this.pocForm.patchValue({ citySelectedFlag: false }) : this.pocForm.patchValue({ citySelectedFlag: true });

      this.pocForm
        .get('officesData')
        .patchValue(Array(this.officesData.length).fill(bool), { emitEvent: false });
    });

    // Subscribe to changes on the city checkboxes
    this.pocForm.get('officesData').valueChanges.subscribe(val => {

      const allSelected = val.every(bool => bool);
      const notSelected = val.every(bool => !bool);

      if (!allSelected && !notSelected)
        this.pocForm.patchValue({ citySelectedFlag: true });


      if (this.pocForm.get('selectAll').value !== allSelected) {
        this.pocForm.get('selectAll').patchValue(allSelected, { emitEvent: false });
      }

      if (notSelected == true) {
        this.pocForm.patchValue({ citySelectedFlag: false });
      }

    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.pocForm.controls; }

  popoverClose() {
    this.pop.hide();
  }

  openMoreDetails() {

    if (this.selectedEmpDetails.name == '') {
      this.errorMesg = "Please Select POC"
      return;
    }

    if (!this.found)
      this.showMoreDetails = true;
    else
      this.errorMesg = "Already Added"
  }

  closeMoreDetails() {
    this.showMoreDetails = false;
    this.pop.hide();
  }

  setClickedItem = (index, emp) => {

    this.errorMesg = ''
    this.selectedEmpDetails = {
      name : emp.name,
      user_id: emp.user_id,
      user_type: emp.user_type
    }
    
    this.selectedItem = index;

    this.found = this.selectedEmp.some(el => el.user_id === this.selectedEmpDetails.user_id);
    if (this.found)
      this.errorMesg = "Already Added"

  }

  onSubmit() {
    this.submitted = true;

    if (this.pocForm.valid) {

      const selectedCity = this.pocForm.value.officesData
        .map((checked, index) => checked ? this.officesData[index].office_id : null)
        .filter(value => value !== null);


      const pocData = {
        user_id: this.selectedEmpDetails.user_id,
        //user_type: this.selectedEmpDetails.user_type,
        name: this.selectedEmpDetails.name,
        poc_type: this.pocForm.value.pocType,
        office: selectedCity,
        poc_description: this.pocForm.value.pocDescription
      }

      this.found = this.selectedEmp.some(el => el.user_id === this.selectedEmpDetails.user_id);

      if (!this.found)
        this.selectedEmp.push(pocData);

      this.updateData.emit(this.selectedEmp);
      this.popoverClose();

    } else {
      return;
    }
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PocPopoverComponent } from './poc-popover.component';

describe('PocPopoverComponent', () => {
  let component: PocPopoverComponent;
  let fixture: ComponentFixture<PocPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PocPopoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PocPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

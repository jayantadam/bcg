/**
 * Project: life BCG
 * File: category-view.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { CreateCategoryService } from '@services/create-category.service';
import { LoggerService } from '@services/logger.service';
import { Offices } from '@models/index';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SpinnerService } from '@services/spinner.service';
import { Store, select } from '@ngrx/store';
import { polls } from '@ngrx/category/reducers/category.reducer';
import {survey} from '@ngrx/category/reducers/survey.reducer'
import {appurl} from '@ngrx/category/reducers/appurl.reducer' 




@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category-view.component.scss']
})
export class CategoryViewComponent implements OnInit, OnChanges {

  pollsquestion$: Observable<polls>;
  survey$: Observable<survey>;
  appurl$: Observable<appurl>;
  
  @Input() module;
  @Input() visibleStatus :boolean;
  @Output() onGetSelectedCategory = new EventEmitter<void>();

  categoryForm: FormGroup;
  submitted = false;

  addCategoryBtnLoading = true;
  dropdownText = 'Select';
  eventDate = 'Calendar';
  eventTime = 'Select Time'
  addGAlleryFlag = false;
  addPollsFlag = false;
  addLogisticsFlag = false;
  showMainImage = false;
  showAddIcon = false;
  createThreadsFlag = false;
  addSurveysFlag = false;
  addAgendaFlag = false;
  popOverIconimage;
  appIcon;
  appMainImage;
  bsConfig: Partial<BsDatepickerConfig>;

  files = []

  categoryData = []
  officeData = [];
  cohortsData = [];
  groupData = [];
  employeeData = [];

  name : string;

  constructor(
    private createCategoryService: CreateCategoryService,
    private loggerService: LoggerService,
    private formBuilder: FormBuilder,
    private spinner: SpinnerService,
    private pollStore: Store<{ polls: polls }>,
    private surveyStore: Store<{ survey: survey }>,
    private appUrlStore: Store<{ appurl: appurl }>
  ) { }

  ngOnInit() {

    this.categoryForm = this.formBuilder.group({
      name: ['',Validators.required],
      enable_enroll: [false],
      start_date: [''],
      end_date: [''],
      start_time: [''],
      end_time: [''],
      calender_type: [''],
      poc: [''],
      office: [''],
      function_cohort: [''],
      group: [''],
      description: ['',Validators.required],
      visible: [this.visibleStatus],
      appMainImage: ['',Validators.required], 
      appIcon: ['',Validators.required],
      poll: [''],
      survey: [''],
      appurl: [''] 
    });

    this.pollsquestion$ = this.pollStore.pipe(select('polls'));
    this.survey$ = this.surveyStore.pipe(select('survey'));
    this.appurl$ = this.appUrlStore.pipe(select('appurl'));

    this.pollsquestion$.subscribe((data) => {
      this.categoryForm.patchValue({ poll: data.pollsData.map(({type,unique_id, ...keepAttrs}) => keepAttrs)})
    });
    this.survey$.subscribe((data) => {
      this.categoryForm.patchValue({ survey: data.surveyData.map(({type,unique_id, ...keepAttrs}) => keepAttrs)})
    });
    this.appurl$.subscribe((data) => {
      console.log(data.appUrlData);
      this.categoryForm.patchValue({ appurl: data.appUrlData.map(({type,unique_id, ...keepAttrs}) => keepAttrs)})
    });

    this.callInitialApis();

  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.loggerService.log("scahn",changes)
    this.visibleStatus = changes.visibleStatus.currentValue;
    // this.categoryForm.patchValue({ visible : changes.visibleStatus.currentValue})
  }

  // convenience getter for easy access to form fields
  get f() { return this.categoryForm.controls; }

  /**
   * callInitialApis fn used to call multiple api at time with the help of forkJoin
   */
  callInitialApis = () => {
    this.spinner.showSpinnerByName('createCategorySpinner');
    let getCategory = this.createCategoryService.getCategory$().pipe(catchError(e => of(null)));
    let getOffices = this.createCategoryService.getOffices$().pipe(catchError(e => of(null)));
    let getFunctionCohort = this.createCategoryService.getFunctionMix$().pipe(catchError(e => of(null)));
    let getGroup = this.createCategoryService.getGroup$().pipe(catchError(e => of(null)));
    let getEmployee = this.createCategoryService.getEmployee$(1,0,1,10).pipe(catchError(e => of(null)));

    forkJoin([getCategory, getOffices, getFunctionCohort, getGroup, getEmployee]).subscribe({
      next: (results) => {

        this.loggerService.log(results)
        if (results[0] != null && results[0].data.length > 0)
          this.categoryData = results[0].data;

        if (results[1] != null && results[1].data.length > 0) {
          this.officeData = results[1].data.map(v => ({ ...v, isActive: false }));
          this.categoryForm.patchValue({ office: this.officeData.map(v => v.office_id) })
        }

        if (results[2] != null && results[2].data.length > 0) {
          this.cohortsData = results[2].data.map(v => ({ ...v, isActive: false }));
          this.categoryForm.patchValue({
            function_cohort: this.cohortsData.map(res => {
              const container = {};
              container['function_id'] = res.function_id;
              container['type'] = res.type;
              return container;
            })
          })
        }

        if (results[3] != null && results[3].data.length > 0) {
          this.groupData = results[3].data.map(v => ({ ...v, isActive: false }));
          this.categoryForm.patchValue({ group: this.groupData.map(v => v.group_id) })
        }

        if (results[4] != null && results[4].data.length > 0)
        this.employeeData = results[4].data;

      },
      error: error => {
        //Set errorlog
        this.loggerService.error(error)
      },
      complete: () => {
        this.spinner.hideSpinnerByName('createCategorySpinner');
      }
    });
  }

  addCategoryBtn() {

  }

  getSelectedCategory(data) {
    this.categoryForm.patchValue({ name: data.name })
    // this.onGetSelectedCategory.emit(data);
  }

  displayMainImage = (data) => {
    this.appMainImage = data.image[0];
    this.categoryForm.patchValue({appMainImage : data.image[0]})
    this.showMainImage = true;
  }

  displayIcon = (data) => {
    this.appIcon = data.image[0];
    this.categoryForm.patchValue({appIcon : data.image[0]})
    this.showAddIcon = true;
  }

  deleteIconMainImage = (val) => {
    if(val == 'main') {
      this.showMainImage=false;
      this.appMainImage = null;
      this.categoryForm.patchValue({appMainImage : ''})
    }else {
      this.showAddIcon=false;
      this.appIcon = null;
      this.categoryForm.patchValue({appIcon : ''})
    }
  }

  displayAppUrl = (data) => {

  }

  getUpdatedFiles = (event) => {
    console.log("updated files===1", event);
  //  this.createCategoryService.uploadGallery$(event).subscribe(json => console.log("image ",json))

  }

  getDate = (event) => {
    this.loggerService.log('Event Date =',event)
    this.eventDate = event.start_date+((event.end_date != null) ? ' - '+event.end_date :'');
    this.categoryForm.patchValue({ 
      start_date: event.start_date,
      end_date: event.end_date,
      calender_type: event.type
    })
  }

  getTime = (event) => {
    this.loggerService.log('Event Time =',event)
    this.eventTime= event.start_time+((event.end_time != null) ? ' - '+event.end_time :'');
    this.categoryForm.patchValue({ start_time: event.start_time,end_time: event.end_time })
  }

  enrolmentChange = (value : boolean) => {
    this.categoryForm.patchValue({ enable_enroll: value })
  }

  getPocData = (pocData) => {
    this.loggerService.log("poc = ",pocData)
    this.categoryForm.patchValue({ poc: pocData })
  }

  getOfficeData = (officeData) => {
    this.loggerService.log("office = ",officeData)
    this.categoryForm.patchValue({ office: officeData })
  }

  getFunctionCohortsData = (functionCohortsData) => {
    this.loggerService.log("functionCohortsData = ",functionCohortsData);
    this.categoryForm.patchValue({ function_cohort: functionCohortsData });
  }

  getGroupData = (groupData) => { 
    this.loggerService.log("groupData = ",groupData);
    this.categoryForm.patchValue({ group: groupData });
  }

  onEditorChange = (editorData) => {
    this.categoryForm.patchValue({ description : editorData});
  }

  getAppUrlData = (data) => {
    this.loggerService.log("App data = ",data)
  }


  onSubmit() {
    this.submitted = true;

    this.loggerService.log("this.pocForm.value", this.categoryForm.value)

    

    const categoryData = {
      name: this.categoryForm.value.name,
      enable_enroll: this.categoryForm.value.enable_enroll,
      description: this.categoryForm.value.description,
      // add_on:[],
      survey: [],
      poll: [],
      office: this.categoryForm.value.office,
      poc: this.categoryForm.value.poc,
      group: this.categoryForm.value.group,
      function_cohort: this.categoryForm.value.function_cohort,
      start_date: this.categoryForm.value.start_date,
      end_date: this.categoryForm.value.end_date,
      start_time: this.categoryForm.value.start_time,
      end_time: this.categoryForm.value.end_time,
      // location: {},
      visible: this.categoryForm.value.visible,
      location: {
        name: "Mumbai",
        latitude: 38.8951,
        longitude: -77.0364
      },
    }

    if (this.categoryForm.valid) {
      this.createCategoryService.addCategory$(categoryData).subscribe(json => console.log("image ",json))
    } else {
      return;
    }
  }
}


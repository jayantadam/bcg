/**
 * Project: life BCG
 * File: category.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { LoggerService } from "@services/logger.service";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  categoryStatus = [
    {
      name : 'Hidden',
      value : false
    },
    {
      name : 'Show',
      value : true
    }
  ]
  sectionTab :boolean = false;
  subSectionTab :boolean = false;
  status : boolean = true;
  preselectedCategoryText  = [{
    name : 'Show',
    value : true
  }];
  @ViewChild('categoryTabs') categoryTabs: TabsetComponent;

  @Output()
  wasCancelled = new EventEmitter();
  constructor(
    private loggerService: LoggerService
  ) { }

  ngAfterViewInit(){
    // this.categoryTabs.tabs[1].disabled = true;
    // this.categoryTabs.tabs[2].disabled = true;
  }


  ngOnInit(): void {
  }

  getSelectedCategoryStatus = (event) => {
    this.preselectedCategoryText[0].name = event.name;
    this.status = event.value;
  }

  getSelectedCategory = (data) => {
    this.loggerService.log('category selected data =',data);
  }

  saveCategoryData = (categoryView) => {
    categoryView.onSubmit()
  }

  cancel() {
    this.wasCancelled.emit();
  }

}

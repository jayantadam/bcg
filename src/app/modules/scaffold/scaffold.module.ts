/**
 * Project: life BCG
 * File: scaffold.module
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScaffoldRoutingModule } from './scaffold-routing.module';
import { ScaffoldComponent } from './containers/scaffold/scaffold.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NotificationbarComponent } from './components/notificationbar/notificationbar.component';
import { SharedModule } from '../../shared/shared.module';
import { CreateCategoryModule } from '../../modules/create-category/create-category.module';
import { RouterModule } from '@angular/router';

import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';

import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';


@NgModule({
  declarations: [
    ScaffoldComponent, 
    SidebarComponent, 
    NotificationbarComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    ScaffoldRoutingModule,
    SharedModule,
    CreateCategoryModule,
    NgSlimScrollModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    BsDropdownModule.forRoot()
  ],
  providers: [{
    provide: SLIMSCROLL_DEFAULTS,
    useValue: {
      alwaysVisible: true,
      barOpacity: '0.5',
      gridBackground: 'transparent',
      barBackground: '#3fad93',
      barWidth: '4',
      barMargin: '4px 4px'
    }
  }],
  exports: [ScaffoldComponent]
})
export class ScaffoldModule { }

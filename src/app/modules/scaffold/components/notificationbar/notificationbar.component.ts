/**
 * Project: life BCG
 * File: notificationbar.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notificationbar',
  templateUrl: './notificationbar.component.html',
  styleUrls: ['./notificationbar.component.scss']
})
export class NotificationbarComponent implements OnInit {

  notificationSubMenus = [
    {
      "title": "All",
      "icon": "icon-notification-icon",
      "count": "110",
      "route" : "home"
    },
    {
      "title": "Reports",
      "icon": "icon-reports-icon",
      "count": "10",
      "route" : "reports"
    },
    {
      "title": "Enrolment",
      "icon": "icon-enrollment-icon",
      "count": "21",
      "route" : "enrolment"
    },
    {
      "title": "Rewards",
      "icon": "icon-rewards-icon",
      "count": "5",
      "route" : "rewards"
    },
    {
      "title": "Feedback",
      "icon": "icon-feedback-icon",
      "count": "32",
      "route" : "feedback"
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}

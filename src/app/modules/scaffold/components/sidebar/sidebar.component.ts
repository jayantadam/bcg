/**
 * Project: life BCG
 * File: sidebar.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { CategoryComponent } from "../../../create-category/modal/category/category.component";
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  data = [{
    "name": "Home",
    "data": []
  }, {
    "name": "Categories",
    "data": [{
      "name": "cat0",
      "data": []
    }, {
      "name": "cat1",
      "data": [{
        "name": "sub cat1",
        "data": []
      }, {
        "name": "sub cat2",
        "data": []
      }]
    }]
  }
  ]

  sidemenuData_ = [
    {
      "name": "Home",
      "icon": "icon-home-icon",
      "type": "menu",
      "data": []
    },
    {
      "name": "Categories",
      "icon": "icon-categories-icon",
      "type": "menu",
      "data": [
        {
          "name": "Extra-Curricular-Activitie",
          "type": "category",
          "icon": "icon-circle",
          "data": [
            {
              "name": "Daily",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Dashboard",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Charts",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Areas",
              "type": "subsection",
              "icon": "",
              "data": []
            }
          ]
        },
        {
          "name": "Schedules",
          "type": "category",
          "icon": "icon-circle",
          "data": [
            {
              "name": "Daily",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Dashboard",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Charts",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Areas",
              "type": "subsection",
              "icon": "",
              "data": []
            }
          ]
        },
        {
          "name": "HR",
          "type": "category",
          "icon": "icon-circle",
          "data": [
            {
              "name": "Recruitment",
              "type": "section",
              "icon": "",
              "data": [
                {
                  "name": "Campus",
                  "type": "subsection",
                  "icon": "",
                  "data": []
                },
                {
                  "name": "Lateral",
                  "type": "subsection",
                  "icon": "",
                  "data": []
                },
                {
                  "name": "BST",
                  "type": "subsection",
                  "icon": "",
                  "data": []
                }
              ]
            },
            {
              "name": "L&D",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Staffing & Mobility",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "PTO",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Engagement",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "New Hire Integration",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "CDC",
              "type": "subsection",
              "icon": "",
              "data": []
            }
          ]
        },

        {
          "name": "Finance",
          "type": "category",
          "icon": "icon-circle",
          "data": [
            {
              "name": "Daily",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Dashboard",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Charts",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Areas",
              "type": "subsection",
              "icon": "",
              "data": []
            }
          ]
        },

        {
          "name": "Global Design Studio",
          "type": "category",
          "icon": "icon-circle",
          "data": [
            {
              "name": "Daily",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Dashboard",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Charts",
              "type": "subsection",
              "icon": "",
              "data": []
            },
            {
              "name": "Areas",
              "type": "subsection",
              "icon": "",
              "data": []
            }
          ]
        }
      ]
    },
    {
      "name": "Calendar",
      "icon": "icon-calendar-icon",
      "type": "menu",
      "data": [
        {
          "name": "Orders",
          "type": "subsection",
          "icon": "icon-circle",
          "data": []
        },
        {
          "name": "Customers",
          "type": "subsection",
          "icon": "icon-circle",
          "data": []
        }
      ]
    },
    {
      "name": "App Management",
      "icon": "icon-app-mngmnt-icon",
      "type": "menu",
      "data": [
        {
          "name": "Orders",
          "type": "subsection",
          "icon": "icon-circle",
          "data": []
        },
        {
          "name": "Customers",
          "type": "subsection",
          "icon": "icon-circle",
          "data": []
        }
      ]
    },
    {
      "name": "Polls",
      "icon": "icon-polls-icon",
      "type": "menu",
      "data": [
        {
          "name": "Orders",
          "type": "subsection",
          "icon": "icon-circle",
          "data": []
        },
        {
          "name": "Customers",
          "type": "subsection",
          "icon": "icon-circle",
          "data": []
        }
      ]
    },
    {
      "name": "Rewards",
      "icon": "icon-rewards-icon-2",
      "type": "menu",
      "data": [
        {
          "name": "Orders",
          "type": "subsection",
          "icon": "icon-circle",
          "data": []
        },
        {
          "name": "Customers",
          "type": "subsection",
          "icon": "icon-circle",
          "data": []
        }
      ]
    },
    {
      "name": "User FAQs",
      "icon": "icon-user-faqs-icon",
      "type": "menu",
      "data": [
        {
          "name": "Orders",
          "type": "subsection",
          "icon": "icon-circle",
          "data": []
        },
        {
          "name": "Customers",
          "type": "subsection",
          "icon": "icon-circle",
          "data": []
        }
      ]
    },
    {
      "name": "Analytics",
      "icon": "icon-analytics-icon",
      "type": "menu",
      "data": [
        {
          "name": "Orders",
          "type": "subsection",
          "icon": "icon-circle",
          "data": []
        },
        {
          "name": "Customers",
          "type": "subsection",
          "icon": "icon-circle",
          "data": []
        }
      ]
    }
  ]
  

  modalRef: BsModalRef;
  constructor(private modalService: BsModalService) {}

  ngOnInit(): void {

  }

  openModal() {
    // this.modalRef = this.modalService.show(template,{class: 'modal-lg cuxtomModal'});
    const bsModalRef = this.modalService.show(
      CategoryComponent,
      {
        class: 'modal-lg customModalLarge',
        ignoreBackdropClick: true
      });

    bsModalRef.content.wasCancelled.pipe(first()).subscribe((result: string) => {
      bsModalRef.hide();
    });
    
  }

}

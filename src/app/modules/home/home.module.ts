/**
 * Project: life BCG
 * File: home.module
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EventCardComponent } from './components/event-card/event-card.component';
import { PostCardComponent } from './components/post-card/post-card.component';
import { TopPollsComponent } from './components/top-polls/top-polls.component';
import { UpcomingActivitiesComponent } from './components/upcoming-activities/upcoming-activities.component';
import { ActiveUsersComponent } from './components/active-users/active-users.component';


@NgModule({
  declarations: [
    HomeComponent, 
    EventCardComponent, 
    PostCardComponent, 
    TopPollsComponent, 
    UpcomingActivitiesComponent, 
    ActiveUsersComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ]
})
export class HomeModule { }

/**
 * Project: life BCG
 * File: active-users.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-active-users',
  templateUrl: './active-users.component.html',
  styleUrls: ['./active-users.component.scss']
})
export class ActiveUsersComponent implements OnInit {

  @Input() data;
  constructor() { }

  ngOnInit(): void {
  }

}

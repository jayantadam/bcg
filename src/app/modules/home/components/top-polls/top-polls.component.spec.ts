import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopPollsComponent } from './top-polls.component';

describe('TopPollsComponent', () => {
  let component: TopPollsComponent;
  let fixture: ComponentFixture<TopPollsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopPollsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopPollsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

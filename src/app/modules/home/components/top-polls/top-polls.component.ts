/**
 * Project: life BCG
 * File: top-polls.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { LoggerService } from "@services/logger.service";
import { ToastsService } from "@services/toasts.service";


@Component({
  selector: 'app-top-polls',
  templateUrl: './top-polls.component.html',
  styleUrls: ['./top-polls.component.scss']
})
export class TopPollsComponent implements OnInit {

  @Input() data;
  constructor(
    private logger : LoggerService,
    private toasts : ToastsService
    ) { }

  ngOnInit(): void {
    this.logger.log("data = ",this.data);

  }

  checkRadioValue = (event) =>{
    this.logger.log(`chnages value = ${event.target.value}`);
    // this.toasts.showSuccess("hello world")
  }

}

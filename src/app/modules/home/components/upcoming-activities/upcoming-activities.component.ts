/**
 * Project: life BCG
 * File: upcoming-activities.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-upcoming-activities',
  templateUrl: './upcoming-activities.component.html',
  styleUrls: ['./upcoming-activities.component.scss']
})
export class UpcomingActivitiesComponent implements OnInit {

  @Input() data;
  constructor() { }

  ngOnInit(): void {
  }

}

/**
 * Project: life BCG
 * File: home.component
 * Author: Sushant Indulkar - sushant.indulkar@wwindia.com
 * -----
 * Copyright (c) 2020
 * -----
 * HISTORY:
 * Date      	By       	Comments
 * ----------	---------	---------------------------------------------------------
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, ChildActivationEnd } from '@angular/router';

import { SpinnerService } from '@services/spinner.service';
import { HttpClient } from '@angular/common/http';
import { LoggerService } from '@services/logger.service';
import { WebService } from "@services/web.service";
import { map, catchError, takeUntil, skip, take } from 'rxjs/operators';
import { of, Subject } from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  addClass = false;
  _title: string = "Home";
  feedData = [
    {
      "eventName": "Cricket",
      "eventTitle": "Annual Cricket Event - 2020",
      "eventDescription": "Celebrating each other; Come leaf the tree with thank you notes for those who have demonstrated BCG values",
      "date": "11 March 2020",
      "time": "10:40 AM",
      "location": "BEK - Maple / BEK - Amazon / BEK - Havana",
      "image": "https://homepages.cae.wisc.edu/~ece533/images/airplane.png",//"assets/images/cricket.jpg",
      "createdDate": "2020-04-10T18:20:25+05:30"
    },
    {
      "eventName": "Football",
      "eventTitle": "Annual Football Event - 2020",
      "eventDescription": "Celebrating other; Come leaf the tree with thank you notes for those who have demonstrated BCG values",
      "date": "31 March 2020",
      "time": "10:40 PM",
      "location": "Mumbai - Maharashtra",
      "image": "https://homepages.cae.wisc.edu/~ece533/images/arctichare.png",//"assets/images/cricket.jpg",
      "createdDate": "2020-04-10T17:01:25+05:30"
    },
    {
      "eventName": "Football",
      "eventTitle": "Annual Football Event - 2020",
      "eventDescription": "Celebrating other; Come leaf the tree with thank you notes for those who have demonstrated BCG values",
      "date": "31 March 2020",
      "time": "10:40 PM",
      "location": "Mumbai - Maharashtra",
      "image": "https://homepages.cae.wisc.edu/~ece533/images/baboon.png",//"assets/images/cricket.jpg",
      "createdDate": "2020-04-10T17:01:25+05:30"
    },
    {
      "eventName": "Football",
      "eventTitle": "Annual Football Event - 2020",
      "eventDescription": "Celebrating other; Come leaf the tree with thank you notes for those who have demonstrated BCG values",
      "date": "31 March 2020",
      "time": "10:40 PM",
      "location": "Mumbai - Maharashtra",
      "image": "https://homepages.cae.wisc.edu/~ece533/images/boat.png",//"assets/images/cricket.jpg",
      "createdDate": "2020-04-10T17:01:25+05:30"
    },
    {
      "eventName": "Football",
      "eventTitle": "Annual Football Event - 2020",
      "eventDescription": "Celebrating other; Come leaf the tree with thank you notes for those who have demonstrated BCG values",
      "date": "31 March 2020",
      "time": "10:40 PM",
      "location": "Mumbai - Maharashtra",
      "image": "https://homepages.cae.wisc.edu/~ece533/images/cat.png",//"assets/images/cricket.jpg",
      "createdDate": "2020-04-10T17:01:25+05:30"
    },
    {
      "eventName": "Football",
      "eventTitle": "Annual Football Event - 2020",
      "eventDescription": "Celebrating other; Come leaf the tree with thank you notes for those who have demonstrated BCG values",
      "date": "31 March 2020",
      "time": "10:40 PM",
      "location": "Mumbai - Maharashtra",
      "image": "https://homepages.cae.wisc.edu/~ece533/images/monarch.png",//"assets/images/cricket.jpg",
      "createdDate": "2020-04-10T17:01:25+05:30"
    },
    {
      "eventName": "",
      "eventTitle": "Annual Cricket Event - 2020",
      "eventDescription": "Celebrating each; Come leaf the tree with thank you notes for those who have demonstrated BCG values",
      "date": "11 March 2020",
      "time": "10:40 AM",
      "location": "",
      "image": "",
      "createdDate": "2020-04-10T19:01:25+05:30"
    }
  ];

  topPolls = [
    {
      "question": "Do you want to attend Gala Event?",
      "options": [
        {
          "optTitle": "Yes",
          "optPercentage": 40,
          "value": "yes"
        },
        {
          "optTitle": "No",
          "optPercentage": 50,
          "value": "no"
        },
        {
          "optTitle": "Others",
          "optPercentage": 10,
          "value": "other"
        }
      ]
    },
    {
      "question": "Help us to choose next location for Annual Outing",
      "options": [
        {
          "optTitle": "Goa",
          "optPercentage": 40,
          "value": "goa"
        },
        {
          "optTitle": "Kerala",
          "optPercentage": 60,
          "value": "kerala"
        }
      ]
    },
    {
      "question": "Proin interdum, nulla in tempor aliquam?",
      "options": [
        {
          "optTitle": "Yes",
          "optPercentage": 40,
          "value": "yes"
        },
        {
          "optTitle": "No",
          "optPercentage": 50,
          "value": "no"
        }
      ]
    }
  ]

  upcoming_activities = [
    {
      "title": "Annual Cricket Event - 2020",
      "location": "Bangalore, India",
      "date": "2020-04-16T18:20:25+05:30",
      "time": "10:00 AM"
    },
    {
      "title": "Annual Football Event - 2020",
      "location": "Bangalore, India",
      "date": "2020-04-15T18:20:25+05:30",
      "time": "9:30 AM"
    },
    {
      "title": "Annual Event - 2020",
      "location": "Mumbai, India",
      "date": "2020-04-22T18:20:25+05:30",
      "time": "6:00 PM"
    },
    {
      "title": "Cricket Event - 2020",
      "location": "Delhi, India",
      "date": "2020-05-30T18:20:25+05:30",
      "time": "10:00 PM"
    }
  ]

  active_users = [
    {
      "name": "Billy Carr.",
      "place": "Mumbai",
      "image": "assets/images/user.png",
      "status": "active",
      "lastVisited": ""
    },
    {
      "name": "Jessica Davis",
      "place": "Boston",
      "image": "assets/images/user.png",
      "status": "inActive",
      "lastVisited": "2020-04-09T18:20:25+05:30"
    },
    {
      "name": "Hemachandra Gandhi Dewan",
      "place": "San Fransisco",
      "image": "assets/images/user.png",
      "status": "active",
      "lastVisited": ""
    },
    {
      "name": "Hailey Lavoie",
      "place": "New Delhi",
      "image": "assets/images/user.png",
      "status": "inActive",
      "lastVisited": "2020-04-13T14:10:25+05:30"
    }
  ]

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private spinner: SpinnerService,
    private http: HttpClient,
    private loggerService: LoggerService,
    private webService: WebService
  ) { }

  ngOnInit() {

    //   this.route.parent.url.subscribe(url => console.log("1",url[0].path));

    // // Current Path:  company 
    // this.route.url.subscribe(url => console.log("2",url[0].path));

    // // Data:  { title: 'Company' } 
    this.spinner.showSpinner();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hideSpinner();
    }, 2000);
    this.route.data.subscribe(data => this._title = data.title);
    // const temp = this.http.get('http://localhost:3000/customers');
    // this.loggerService.log(temp)



    const gotResponse$ = new Subject();
    // this.webService.getApi('/posts').pipe(
    //         map((temp) => this.loggerService.log(temp)),
    //         catchError(() => of({ type: '[Movies API] Movies Loaded Error' }))
    //       )

    // this.webService.getApi('https://jsonplaceholder.typicode.com/posts').pipe(takeUntil(gotResponse$)).subscribe(user => {
    //   this.loggerService.log("first ==", user)
    //   gotResponse$.next();
    // })

    // this.webService.getApi('http://localhost:3000/customers').pipe(takeUntil(gotResponse$)).subscribe(user => {
    //   this.loggerService.log("first ====", user)
    //   gotResponse$.next();
    // })

    // this.webService.getApi('https://jsonplaceholder.typicode.com/posts').subscribe((data) => {
    //   this.loggerService.log(data)
    // });

    //http://dummy.restapiexample.com/api/v1/employees
    // this.webService.getApi('https://jsonplaceholder.typicode.com/posts').subscribe({
    //   next: user => { this.loggerService.log(user) },
    //   error: error => {
    //     this.loggerService.error(error)
    //   },
    //   complete() { console.log('Completed'); }
    // });

    // this.webService.postEmployee('http://dummy.restapiexample.com/api/v1/create', { "name": "test", "salary": "123", "age": "23" }).subscribe({
    //   next: (user) => { this.loggerService.log(user) },
    //   error: error => {
    //     this.loggerService.error(error)
    //   },
    //   complete() { console.log('Completed'); }
    // });



  }

}
